import React, { SyntheticEvent } from 'react';
import './index.scss';

import Icon from '../components/icon';
import Dropdown, { Option } from '../components/dropdown';

export default function Index () {
    const options: Option[] = [
        {
            caption: 'Esimine',
            value: 'esimine',
        },
        {
            caption: 'Teine',
            value: 'teine',
        },
        {
            caption: 'Disabled',
            value: 'disabled',
            disabled: true,
        },
        {
            caption: <><Icon glyph="weather"/> Ilm </>,
            value: 'ilm',
        },
        {
            caption: 'Customer support',
            value: 'customer-support',
            className: '-special',
        },
    ];

    const selected = (value: string, event: SyntheticEvent) => console.log('%c Dropdown selected ', 'background: springgreen', value);

    return (
        <>
            <header>
                <h1>Arendaja</h1>
                <h2>Prooviülesanne</h2>
            </header>
            <main>
                <h3>Dropdown selector</h3>
                <section>
                    <Dropdown options={options} selected={selected}>
                        Erinevad toimingud <Icon glyph="arrow-right" align="right"></Icon>
                    </Dropdown>
                </section>
            </main>
        </>
    );
}
