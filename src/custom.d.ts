declare module '*.svg?inline' {
    const content: React.FunctionComponent<React.SVGAttributes<SVGElement>>;

    export default content;
}

declare module '*.svg' {
    export default string;
}
