import React from 'react';
import './index.scss';
import './icons.svg';

type Props = {
    glyph: string;
    align?: string;
}

export default function Icon ({ glyph, align }: Props) {
    const href = 'images/icons.svg#' + glyph;
    const className = 'icon' + (align ? ' -' + align : ''); 

    return (
        <svg className={className} focusable="false">
            <use xlinkHref={href} href={href}></use>
        </svg>
    );
}
