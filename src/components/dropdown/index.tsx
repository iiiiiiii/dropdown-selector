import React, { useState, SyntheticEvent } from 'react';
import { Wrapper, Button, Menu, MenuItem } from 'react-aria-menubutton';

type Props = {
    options: Option[],
    selected: (value: any, event: SyntheticEvent<HTMLElement, Event>) => any,
    children?: React.ReactNode,
};

export type Option = {
    caption: React.ReactNode,
    value?: string,
    className?: string,
    disabled?: boolean,
};

function Dropdown({ options, selected, children }: Props) {
    const [value, setValue] = useState('');
    const select = (value: any, event: SyntheticEvent<HTMLElement, Event>) => {
        const target = event.target as HTMLElement;
    
        if (target.getAttribute('aria-disabled')) {
            return;
        }

        setValue(value);
        selected(value, event);
    }
    const items = options.map((option, i) => (
        <MenuItem tag='li' key={i} value={option.value} aria-disabled={option.disabled} data-selected={option.value === value ? 'selected' : undefined} className={option.className ? option.className : undefined}>
            {option.caption}
        </MenuItem>
    ));

    return (
      <Wrapper
        tag="ui-dropdown"
        onSelection={select}
      >
        <Button tag="button">
            {children}
        </Button>
        <Menu tag='ui-options'>
            <ul>{items}</ul>
        </Menu>
      </Wrapper>
    );
}

export default Dropdown;
