import React from 'react';
import ReactDOM from 'react-dom';

import './index.scss';
import './index.js';

import Index from './layouts';

ReactDOM.render(
    <React.StrictMode>
        <Index />
    </React.StrictMode>,
    document.querySelector('body')
);
