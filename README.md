# Dropdown selector component

A simple dropdown element built with React, Typescript and accessibility in mind.

Powered by [react-aria-menubutton](https://github.com/davidtheclark/react-aria-menubutton).

## Install
```shell
npm install
```

## Run
```shell
webpack serve --port 3000
```

## Build
```shell
NODE_ENV=production webpack
```

## Serve build
To prevent SVG loading issues when opening directly from file it is encouraged to use a server. A simple ```http-server``` would suffice:
```shell
npm install --global http-server
```
```shell
http-server build
```

## Usage
```tsx
const options: Option[] = {
    {
        caption: 'Option A',
        value: 'option-a',
    },
    {
        caption: 'Option B',
        value: 'option-b',
    },
}

const selected = (value: string, event: React.SyntheticEvent) => console.log(value);

<Dropdown options={options} selected={selected}>
    Button caption
</Dropdown>
```

### ```<Dropdown />``` attributes

#### ```options: Option[] ``` (required)
An array of ```Option``` objects.

#### ```selected: (value: any, event: SyntheticEvent<HTMLElement, Event>) => any``` (required)
Callback function triggered when an option is selected.

### An ```Option``` object
Each option can have ```caption```, (that are required) and optional ```value```, ```className``` and ```disabled``` properties:

```ts
type Option = {
    caption: React.ReactNode,
    value?: string,
    className?: string,
    disabled?: boolean,
};
```

### ```Option``` properties

#### ```caption: React.ReactNode ``` (required)
Used to display text in the option pop up menu and used as value if not redifined with ```value``` property.

#### ```value?: string```
Used to set the value of selected option.

#### ```className?: string```
Used to set speceific class name

#### ```disabled?: boolean```
Used to mark if option is disabled for selection.

## Specification
This component is built with the help of ```react-aria-menubutton``` component that provides keyboard interactions and ARIA attributes (see https://github.com/davidtheclark/react-aria-menubutton for more details).

The reason behind this is to speed up the development process as it matches the requirements quite fairly. There are some drawbacks with using this particular component, please see [Know proplems](#known-problems) section below.

## `<Icon />` component

To demonstrate usage of icons inside component, an icon component has been added.

```ts
type Props = {
    glyph: string;
    align?: string;
}
```

### ```<Icon />``` attributes

#### ```glyph: string ``` (required)
Used to point for the SVG symbol of corresponding icons file,

#### ```align?: string```
Used for style ajustment when used inside ```<button />``` element.

```left``` and ```right``` are currently supported.

```tsx
<Icon glyph="heart" />
```

Or inside a button:
```tsx
<button><Icon glyph="like" align="left" /> Like</button>
```

## Styles
The components introduced in this project have no or very few styles. This is made with intention to separate functionality and presentation. The latter is usualy tied with branding and other global and local concerns on how the component should look in context. To avoid this and to be close to standard HTML elements in some way, styling are applied separately in global CSS file.

This means that if, for example, a ```<button />``` element has styles, it has the same styles everywhere it is used, because all buttons look and act the same way and can be set once.

Note: In this project styles are applied directly to HTML elements, which might not be ideal and useful in some real situations. Stylings can (and sometimes should) be applied with classes and attributes, but the point is to have them styled once in shared styles and then used with the right markup.

Layouts have styles that only related to layout parts: header, footer (not in this example), main etc.

## Taken liberties
As there was no design provided for selected option it was desided to use inverted (color/background) for that matter.

Some style hints (colors, sizes), fonts and icons were partially imported to boost development process and increase similarity to the original branding and the provided design mockup.

## Known problems
* Eventhough option elements are marked as ```aria-disabled="true"``` attribute, it still can be focused via keyboard. It is described as recommended solution in https://www.w3.org/TR/wai-aria-practices/#kbd_disabled_controls, however it is strange as usually native controls, selects and menus skip the disabled element when navigating with keyboard arrows.

* Clicking on disabled element closes the dropdown which might be confusing as it dows not select the disabled option. Same happens when navigated by keyboard and pressing space/return keys.

* When open with keyboard spacebar or the enter key, the first element is always selected. If the same buttons are pressed again (which can happen by mistake), wrong option could be selected. Ideally the selected option should be selectd or none when opened first time.

* Dropdown menu always appears in the bottom of the button, not ideal when dropdown is located in bottom of the page as options are being cut from view.

* No adoptation for small screens/mobile media style/script-wise.

* Icon ```align``` attribute is currently working only inside the ```<button />``` element which looks more like a hack than a proper solution. Ideally no ```align``` attribute should be required for this purpose.

## Possible improvements
Besides solving all of issues pointed in the previous section there are couple of thoughts on how to make the component more usable.

* Along with using the config for dropdown options, making custom ```<Option />``` component might be more convinient is some cases. Maybe even putting dropdown caption inside the attribute so it would look like:
```tsx
<Dropdown caption={<>Favorites <Icon glyph="star" /></>}>
    <Option>Option A</Option>
    <Option><Icon glyph="rocket" /> Option B</Option>
</Dropdown>
```
Or event better:
```tsx
<Dropdown>
    <Caption>Favorites <Icon glyph="star" /></Caption>
    <Option>Option A</Option>
    <Option><Icon glyph="rocket" /> Option B</Option>
</Dropdown>
```
* SCSS/CSS variables could be used present styles, however for ```<Dropdown />``` being mostly functional component, might not too relevant for this project.

* Selected element could be represented after it was chosen by user. Currently it is unclear visually if anything was selected.

* UX and low-level UI wise, dropdown could be opened by ```mousedown``` event as it behaves on native ```<select />``` elements and desktop application menus. Any selection can be made with single click: press down, menu open, navigate cursor to desired option/menu item, release the mouse and selection is made.

* Tests coverage would be nice.
